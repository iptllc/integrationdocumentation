###notice response

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
CustomerCaseNumber |	N |	string |	Unique string identifying the case number created at the client for this notice record.
Message |	N |	string |	String to include any communication such as an error message.
Success |	Y |	Boolean |	Boolean value notifying paylock if the request was successfully received
