###notice request

Property |	Required |	Type |	Description
---------|-----------|-------|-------------
ClientCode |	Y |	string |	3 letter code for the client municipality
Location |	Y |	string |	Location where the notice action happened
NoticeDate |	Y |	dateTime |	DateTime when the notice was placed on the vehicle
NoticeFeeAmount |	Y |	decimal |	Paylock's boot fee being charged to the motorist
NoticeType |	N |	string |	Type of noticed applied to the vehicle
Password |	Y |	string |	Authentication Password
PaylockCaseNumber |	Y |	string |	Unique string identifying the case number created by Paylock in order to keep track of the Notice attempt (50 characters)
Plate |	Y |	string |	Noticed vehicle license plate
PlateType |	N |	string |	Noticed vehicle plate type
State |	Y |	string |	Noticed vehicle state
UserId |	Y |	string |	Authentication User Id
