###Tticket inquiry request dto

Payload send by Bootview to the ticket inquiry service.  

--------------

This service will gather all debt related to the specified vehicle, even collateralized debt when applicable, and return it to Bootview's application.  

---------

A Ticket inquiry request can be triggered by multiple actions within Bootview's application.  

--------

Example actions are before applying a boot or before collecting payment, as well as upon a request from our partner to gather the latest information.

--------

Property | Required | Type |Description 
---------|----------|------|------------
UserId	|Y	|string	|Authentication User Id
Password	|Y	|string	|Authentication Password
ClientCode	|Y	|string	|3 letter code for the client municipality
Plate	|Y	|string	|Booted vehicle license plate
State	|Y	|string	|Booted vehicle state
PlateType	|N	|string	|Booted vehicle plate type


