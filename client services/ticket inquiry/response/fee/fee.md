###fee dto

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
CustomerAccountNumber |	N |	string  |	Used for other unique identifiers besides the Fee reference id field (if client keeps different identifiers, this is the place for them)
Description |	N |	 string |	Fee Description
FeeAmount |	Y |	decimal |	current fee amount. This is not the amount due. e.g. ticket = $100, late fee = $20, and $30 partial payment. This field will have $100 + $20 = $120. The partial payment will be shown on the payments collection. The amount due is calculated by Paylock based on the ticket amount minus any payments
FeeReferenceId |	Y |	string |	Fee's unique Identifier
FeeType |	Y |	string |	Type of fee such as boot, notice, tow, late fee, etc
Payments |	N |	 List<[Payment](../payment/payment.md))> |	List of any payments currently applied to this specific fee
