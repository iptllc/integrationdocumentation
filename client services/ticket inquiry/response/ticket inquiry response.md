###ticket inquiry response dto
Payload response to Bootview's ticket inquiry request. 

--------

It describes the vehicle, any debt related to the vehicle or owner that are currently collectable, as well as all payments known

---------

Property | Required | Type | Description
---------|----------|------|-------------
Vehicle	|Y	|[Vehicle](./vehicle/vehicle.md)	|Vehicle object containing information about the vehicle
TicketList	|Y	|List<[Ticket](./ticket/ticket.md)>	|List of ticket objects containing information about the vehicle's tickets (red light ticket, parking ticket, etc)
FeeList	|Y	|List<[Fee](./fee/fee.md)>	|List of fee objects containing information about the vehicle's fees (boot fee, notice fee, tow fee, etc)
Message	|N	|string	|String to include any communication such as an error message.
Success	|Y	|boolean	|Boolean value notifying paylock if the request was successfully received. A false value indicates a problem with the request

