###tow response

Property |	Required |	Type |	Description
---------|-----------|-------|-------------
Success |	Y |	boolean |	Boolean value notifying paylock if the request was successfully received
Message |	N |	string |	String to include any communication such as an error message.
CustomerCaseNumber |	N |	string |	Unique string identifying the case number created at the client for this Tow record
