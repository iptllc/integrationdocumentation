###tow request

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
ClientCaseNumber |	Y |	string |	Unique string identifying the case number by created by the client
ClientCode |	Y |	string |	3 letter code for the client municipality
Password |	Y |	string |	Authentication Password
PaylockCaseNumber |	Y |	string |	Unique string identifying the case number created by Paylock
Plate |	Y |	string |	Towed vehicle license plate
PlateType |	N |	string |	Towed vehicle plate type
State |	Y |	string |	Towed vehicle state
TowCompany |	Y |	string |	Name of the tow company
TowDate |	Y |	dateTime |	DateTime when the Tow happened
TowLocation |	Y |	string |	Location where the Tow action happened
UserId |	Y |	string |	Authentication User Id
