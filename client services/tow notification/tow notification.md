##tow notification

Web service to be notified about a Tow record.

Request|Response
-------|--------
[TowRequestDto](./tow request.md)|	[TowResponseDto](./tow response.md)