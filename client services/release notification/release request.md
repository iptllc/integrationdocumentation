###release request

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
ClientCode |	Y |	string |	3 letter code for the client municipality
UserId |	Y |	string |	Authentication User Id
Password |	Y |	string |	Authentication Password
PaylockCaseNumber |	Y |	string |	Unique string identifying the case number created by Paylock
CustomerCaseNumber |	N |	string |	Unique string identifying the case number created at the client for this record
Plate |	Y |	string |	Released vehicle license plate
State |	Y |	string |	Released vehicle state
PlateType |	N |	string |	Released vehicle plate type
Location |	N |	string |	Location where the release action happened
ReleaseDate |	Y |	dateTime |	DateTime when the release happened
ReleaseReason |	Y |	string |	Reason for the vehicle release (paid, runaway, towed, etc)
ReleaseType |	Y |	string |	values are BOOT or TOW, they denote the type of collection attempt that the vehicle is being released from
