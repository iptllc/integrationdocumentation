###release response

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
Message |	N |	string |	String to include any communication such as an error message.
Success |	Y |	boolean |	Boolean value notifying paylock if the request was successfully received
