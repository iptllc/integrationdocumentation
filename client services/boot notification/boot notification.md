##boot notification

Paylock initiates this communication to notify our partners of a vehicle booted event notification

--------------

Request |	Response 
--------|------------
[BootNotificationRequestDto](./boot notification request.md) |	[BootNotificationResponseDto](./boot notification response.md) 
