###boot notification response

Property |	Required |	Type |	Description
---------|-----------|-------|--------------
Success |	Y |	Boolean |	Boolean value notifying paylock if the request was successfully received
Message |	N |	string |	String to include any communication such as an error message.
CustomerCaseNumber |	N |	string |	Unique string identifying the case number created at the client for this boot record.
