#Paylock Real time integration

For both systems to communicate, the process is fairly simple.  At a glance, Paylock will request debt and payment information as needed.  

-------------
We call this a ticket inquiry and is probably the most important piece of communication between our systems.  
The idea is very simple, we give your system a vehicle to search for, and your system returns the vehicle, a collection of violations, and a collection of payments.  
This information is enough for Paylock's Bootview application to determine the correct amount owed by a motorist at the time of booting, or before taking a payment.

---------

Other types of communication will probably happen, such as letting your system know when Bootview collects a payment, boots a vehicle, or any other type of event deemed important to keep our systems in sync.

--------
Communication can be initiated from your end as well.  Let's say that you took payment on a vehicle that we currently have booted.  You can communicate that event to our system and we in exchange will perform a ticket inquiry to get the latest information from your system.